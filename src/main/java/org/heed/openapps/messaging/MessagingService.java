package org.heed.openapps.messaging;

public interface MessagingService {

	void sendEmail(String emailAddress, String text, String html, String from, String subject);
	
}
