package org.heed.openapps.data;
import java.io.Serializable;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;


public class Sort implements Serializable {
	private static final long serialVersionUID = 3425752653677079454L;
	private String field;
	private String type;
	private boolean reverse;
	private Object sortField;
	
	public static final int SCORE = 0;
	public static final int DOC = 1;
	public static final int DATE = 2;
	public static final int STRING = 3;	
	public static final int INTEGER = 4;
	public static final int FLOAT = 5;
	public static final int LONG = 6;
	public static final int DOUBLE = 7;
	
	
	public Sort() {}
	public Sort(String type, String field, boolean reverse) {
		this.type = type;
		this.field = field;
		this.reverse = reverse;
	}
	
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public boolean isReverse() {
		return reverse;
	}
	public void setReverse(boolean reverse) {
		this.reverse = reverse;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Object getSortField() {
		return sortField;
	}
	public void setSortField(Object sortField) {
		this.sortField = sortField;
	}
	
	public void fromJsonObject(JsonObject object) {
		if(object.containsKey("field")) setField(object.getString("field"));
		if(object.containsKey("type")) setType(object.getJsonString("type").getString());
		if(object.containsKey("reverse")) setReverse(object.getBoolean("reverse"));
	}
	public JsonObject toJsonObject() {
		JsonObjectBuilder builder = Json.createObjectBuilder();	
		builder.add("field", getField());
		builder.add("type", getType());
		builder.add("reverse", isReverse());
		return builder.build();
	}
}
