package org.heed.openapps.fx;

import javafx.scene.layout.StackPane;

public abstract class FormControl extends StackPane {
	
	public abstract void setValue(String value);
	public abstract String getValue();
	
}
