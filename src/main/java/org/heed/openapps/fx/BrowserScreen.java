package org.heed.openapps.fx;

import org.heed.openapps.events.ActionListener;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public abstract class BrowserScreen extends Screen {
	private ActionListener listener;
	
	public abstract void initialize();
	public abstract int back();
	public abstract void stop();
	public abstract void shutdown();
	public abstract void loadHTML(String html);
	public abstract void loadUrl(String url);
	public abstract String getSelectedText();
	
	
	public BrowserScreen() {
		setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
		VBox.setVgrow(this, Priority.ALWAYS);
		HBox.setHgrow(this, Priority.ALWAYS);
		setAlignment(Pos.TOP_LEFT);
		
	}
	
	private SimpleObjectProperty<String> url = new SimpleObjectProperty<String>();    
    public ReadOnlyObjectProperty<String> urlProperty() { return url; }
    public void setUrl(String url) { this.url.set(url); }
    public String getUrl() { return url.get(); }
    
	public ActionListener getActionListener() {
		return listener;
	}
	public void setActionListener(ActionListener listener) {
		this.listener = listener;
	}
		
}
