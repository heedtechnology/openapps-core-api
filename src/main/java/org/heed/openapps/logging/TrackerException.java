package org.heed.openapps.logging;

public class TrackerException extends Exception {
	private static final long serialVersionUID = 2332840450762706664L;

	
	public TrackerException(final String message) {
		super(message);
	}
	
	public TrackerException(final String message, final Throwable e) {
		super(message, e);
	}
}