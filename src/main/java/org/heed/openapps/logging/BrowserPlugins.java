package org.heed.openapps.logging;

public enum BrowserPlugins {

	/**
	 *  Browser plugins.
	 */
	FLASH("fla"), JAVA("java"), DIRECTOR("dir"), QUICKTIME("qt"),
	REALPLAYER("realp"), PDF("pdf"), WINDOWSMEDIA("wma"), GEARS("gears"),
	SILVERLIGHT("ag");

	/**
	 * The short URL.
	 */
	private String urlshort;

	/**
	 * Constructor that sets the short URL.
	 * @param urlshort 
	 */
	BrowserPlugins(final String urlshort) {
		this.urlshort = urlshort;
	}

	@Override
	public String toString() {
		return this.urlshort + "=true";
	}
}