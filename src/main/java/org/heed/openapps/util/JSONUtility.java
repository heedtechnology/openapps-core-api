package org.heed.openapps.util;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

import org.heed.openapps.data.json.JsonReader;

public class JSONUtility {

	
	public static String escape(String in) {
		if(in != null) {
			in = in.replace(",", "\\,");
			in = in.replace("'", "\\'");
			return in;
		}
		return "";
	}
	public static String unescape(String in) {
		if(in != null) {
			in = in.replace("&lt;", "<");
			in = in.replace("&amp;", "&");
			in = in.replace("&apos;", "'");
			in = in.replace("&quot;", "\"");
			//in = in.replaceAll("&#37;", "%");
			return in;
		} 
		return "";
	}
	@SuppressWarnings("unchecked")
	public static Map<String, Object> parse(String in) {
		Map<String, Object> map = (Map<String, Object>)JsonReader.toMaps(in);
		return map;
	}
	public static String quote(String string) {
        StringWriter sw = new StringWriter();
        synchronized (sw.getBuffer()) {
            try {
                return quote(string, sw).toString();
            } catch (IOException ignored) {
                // will never happen - we are writing to a string writer
                return "";
            }
        }
    }
	public static Writer quote(String string, Writer w) throws IOException {
        if (string == null || string.length() == 0) {
            w.write("\"\"");
            return w;
        }
        char b;
        char c = 0;
        String hhhh;
        int i;
        int len = string.length();

        w.write('"');
        for (i = 0; i < len; i += 1) {
            b = c;
            c = string.charAt(i);
            switch (c) {
            case '\\':
            case '"':
                w.write('\\');
                w.write(c);
                break;
            case '/':
                if (b == '<') {
                    w.write('\\');
                }
                w.write(c);
                break;
            case '\b':
                w.write("\\b");
                break;
            case '\t':
                w.write("\\t");
                break;
            case '\n':
                w.write("\\n");
                break;
            case '\f':
                w.write("\\f");
                break;
            case '\r':
                w.write("\\r");
                break;
            default:
                if (c < ' ' || (c >= '\u0080' && c < '\u00a0')
                        || (c >= '\u2000' && c < '\u2100')) {
                    w.write("\\u");
                    hhhh = Integer.toHexString(c);
                    w.write("0000", 0, 4 - hhhh.length());
                    w.write(hhhh);
                } else {
                    w.write(c);
                }
            }
        }
        w.write('"');
        return w;
    }

}
