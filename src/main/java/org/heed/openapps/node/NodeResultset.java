package org.heed.openapps.node;

import java.util.ArrayList;
import java.util.List;

public class NodeResultset {
	private List<Long> ids = new ArrayList<Long>();
	private int total;
	
	
	public List<Long> getIds() {
		return ids;
	}
	public void setIds(List<Long> ids) {
		this.ids = ids;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
		
}
