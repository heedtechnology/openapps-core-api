package org.heed.openapps.node;

public interface Transaction
{
    /**
     * Marks this transaction as failed, which means that it will
     * unconditionally be rolled back when {@link #finish()} is called. Once
     * this method has been invoked, it doesn't matter if
     * {@link #success()} is invoked afterwards -- the transaction will still be 
     * rolled back.
     */
    void failure();

    /**
     * Marks this transaction as successful, which means that it will be
     * committed upon invocation of {@link #finish()} unless {@link #failure()}
     * has or will be invoked before then.
     */
    void success();

    /**
     * Commits or marks this transaction for rollback, depending on whether
     * {@link #success()} or {@link #failure()} has been previously invoked.
     * 
     * All {@link ResourceIterable ResourceIterables} that where returned from operations executed inside this 
     * transaction will be automatically closed by this method.
     */
    void finish();
    
}