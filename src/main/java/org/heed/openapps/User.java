package org.heed.openapps;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.json.JsonWriter;


public class User {
	private Long id;
	private Long xid;
	private String username;
	private String firstName;
	private String lastName;
	private String address;
	private String city;
	private String province;
	private String country;
	private String postalCode;
	private String phoneNumber;
	private String email;
	private String website;
	private String avatar;
	private String password;
	private boolean enabled;
	private List<Role> roles = new ArrayList<Role>();
	private List<Group> groups = new ArrayList<Group>();
	private List<Permission> permissions = new ArrayList<Permission>();
	
	
	public boolean hasRole(String name) {
		for(Role r : roles) {
			if(r.getName().equals(name))
				return true;
		}
		return false;
	}
	public boolean isAdministrator() {
		for(Role r : getRoles()) {
			if(r.getName().equals("Administrator"))
				return true;
		}
		return false;
	}
	public boolean isGuest() {
		return false;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getXid() {
		return xid;
	}
	public void setXid(Long xid) {
		this.xid = xid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}	
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	public List<Group> getGroups() {
		return groups;
	}
	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}
	public List<Permission> getPermissions() {
		return permissions;
	}
	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}
	
	public String toJson() {
		StringWriter writer = new StringWriter();
		JsonWriter jsonWriter = Json.createWriter(writer);				
		JsonObject obj = toJsonObject();		
		jsonWriter.write(obj);
		return writer.toString();
	}
    public JsonObject toSimpleJsonObject() {
    	JsonObjectBuilder builder = Json.createObjectBuilder();		
		if(getId() != null) builder.add("id", getId());
		if(getXid() != null) builder.add("xid", getXid());
		if(getUsername() != null) builder.add("username", getUsername());
		if(getEmail() != null) builder.add("email", getEmail());
		return builder.build();
    }
	public JsonObject toJsonObject() {
		JsonObjectBuilder builder = Json.createObjectBuilder();		
		if(getId() != null) builder.add("id", getId());
		if(getXid() != null) builder.add("xid", getXid());
		if(getUsername() != null) builder.add("username", getUsername());
		if(getEmail() != null) builder.add("email", getEmail());
		JsonArrayBuilder roleBuilder = Json.createArrayBuilder();
		for(Role role : roles) {
			roleBuilder.add(role.toSimpleJsonObject());
		}
		builder.add("roles", roleBuilder);
		JsonArrayBuilder groupBuilder = Json.createArrayBuilder();
		for(Group group : groups) {
			groupBuilder.add(group.toJsonObject());
		}
		builder.add("groups", groupBuilder);
		JsonArrayBuilder permissionBuilder = Json.createArrayBuilder();
		for(Permission permission : permissions) {
			permissionBuilder.add(permission.toJsonObject());
		}
		builder.add("groups", groupBuilder);
		return builder.build();
	}
	public void fromJson(JsonObject object) {
		if(object.containsKey("id")) setId(object.getJsonNumber("id").longValue());
		if(object.containsKey("xid")) setXid(object.getJsonNumber("xid").longValue());
		if(object.containsKey("username")) setUsername(object.getString("username"));
		if(object.containsKey("roles")) {
			JsonArray roles = object.getJsonArray("roles");
			for(JsonValue seedValue : roles) {
				Role role = new Role();
				role.fromJson((JsonObject)seedValue);
				getRoles().add(role);
			}
		}
		if(object.containsKey("groups")) {
			JsonArray groups = object.getJsonArray("groups");
			for(JsonValue seedValue : groups) {
				Group group = new Group();
				group.fromJson((JsonObject)seedValue);
				getGroups().add(group);
			}
		}
	}
}
