package org.heed.openapps.platform;
/**
 * The device service provides generic properties of the device on which the application is running.
 *
 * <p><b>Example</b></p>
 * <pre>
 * {@code Services.get(DeviceService.class).ifPresent(service -> {
 *      System.out.printf("Device Model Name: %s", service.getModel());
 *  });}</pre>
 *
 * <p><b>Android Configuration</b>: none</p>
 * <p><b>iOS Configuration</b>: none</p>
 *
 * @since 3.0.0
 */
public interface DeviceService {

    /**
     * Returns the name of the device's model or product. The value is set by the device
     * manufacturer and may be different across versions of the same product.
     *
     * @return The device model.
     */
    String getModel();

    /**
     * Returns the device's universally unique identifier.
     *
     * @return The device UUID.
     */
    String getUuid();

    /**
     * Returns the platform string that the operating system uses to identify itself.
     *
     * @return The device platform.
     */
    String getPlatform();

    /**
     * Returns the version number of the device platform.
     *
     * @return The device version.
     */
    String getVersion();

    /**
     * Returns the device hardware serial number.
     *
     * @return The device serial.
     */
    String getSerial();
}