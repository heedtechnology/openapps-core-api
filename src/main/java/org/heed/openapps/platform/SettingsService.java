package org.heed.openapps.platform;
/**
 * The SettingService provides a simple way for storing, retrieving and removing
 * key-value pairs of strings.
 *
 * <p><b>Example</b></p>
 * <pre>
 * {@code Services.get(SettingsService.class).ifPresent(service -> {
 *      service.store("key", "value");
 *      String value = service.retrieve("key");
 *      service.remove("key");
 *  });}</pre>
 *
 * <p><b>Android Configuration</b>: none</p>
 * <p><b>iOS Configuration</b>: none</p>
 *
 * @since 3.0.0
 */
public interface SettingsService {

    /**
     * Stores the setting with the specified key and value. If a setting with
     * the specified key exists, the value for that setting will be overwritten
     * with the specified value.
     *
     * @param key a key that uniquely identifies the setting
     * @param value the value associated with the key
     */
    void store(String key, String value);

    /**
     * Removes the setting for the specified key.
     *
     * @param key the key of the setting that needs to be removed
     */
    void remove(String key);

    /**
     * Retrieves the value of the setting that is identified by the specified
     * key.
     *
     * @param key the key of the setting to look up
     * @return the value associated with the setting or <code>null</code> when
     * no setting was stored with the specified key
     */
    String retrieve(String key);

}