package org.heed.openapps.platform;
import java.util.ArrayList;
import java.util.List;

import javafx.stage.Stage;


public class ServiceManager {
	private List<ServiceListener> listeners = new ArrayList<ServiceListener>();
	private PlatformService platformService;
	private SettingsService settingsService;
	private StorageService storageService;
	private DeviceService deviceService;
	private Stage stage;
	
	
	public void subscribe(ServiceListener subscriber) {
		listeners.add(subscriber);
	}
	public void unsubscribe(ServiceListener subscriber) {
		listeners.remove(subscriber);
	}
	public void publish(String action, String msg) {
		for(ServiceListener listener : listeners) {
			listener.fire(action, msg);
		}
	}
	
	public Stage getStage() {
		return stage;
	}
	public void setStage(Stage stage) {
		this.stage = stage;
	}
	public PlatformService getPlatformService() {
		return platformService;
	}
	public void setPlatformService(PlatformService platformService) {
		this.platformService = platformService;
	}
	public SettingsService getSettingsService() {
		return settingsService;
	}
	public void setSettingsService(SettingsService settingsService) {
		this.settingsService = settingsService;
	}
	public StorageService getStorageService() {
		return storageService;
	}
	public void setStorageService(StorageService storageService) {
		this.storageService = storageService;
	}
	public DeviceService getDeviceService() {
		return deviceService;
	}
	public void setDeviceService(DeviceService deviceService) {
		this.deviceService = deviceService;
	}	
	
}
