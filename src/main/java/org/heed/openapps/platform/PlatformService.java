package org.heed.openapps.platform;

import org.heed.openapps.fx.BrowserScreen;
import org.heed.openapps.fx.FormControl;


public interface PlatformService {
	
	void initialize();
	String getName();
	void sendEmail(MailRequest request);
	void sendNotification(String msg);
	void playSoundEffect(String sound);
	void playSound(String sound);
	
	//Double[] getPosition();
		
	BrowserScreen getBrowserScreen(boolean lightweight);
	FormControl getCodeEditor();
	
}
