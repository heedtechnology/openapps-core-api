package org.heed.openapps.platform;
import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;


public class MailRequest {
	private String comment;
	private String body;
	private String subject;
	private String title;
	private String url;
	private String summary;
	private List<String> users = new ArrayList<String>();
	
	
	public MailRequest() {}
	public MailRequest(String body, String subject, String title) {
		this.body = body;
		this.subject = subject;
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}

	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public List<String> getUsers() {
		return users;
	}
	public void setUsers(List<String> users) {
		this.users = users;
	}
	public void fromJson(JsonObject object) {
		if(object.containsKey("subject")) setSubject(object.getString("subject"));
		if(object.containsKey("body")) setBody(object.getString("body"));
		
	}
	public JsonObject toJsonObject() {
		JsonObjectBuilder builder = Json.createObjectBuilder();
		if(body != null) builder.add("body", body);
		if(subject != null) builder.add("subject", subject);
			
		return builder.build();
	}
	public String toUrl() {
		String url = "mailto:";
		for(int i=0; i < users.size(); i++) {
			url += users.get(i);
			if(i < users.size()) url += ",";
		}
		url += "?";
		if(body != null) url += "body="+body+"&";
		if(subject != null) url += "subject="+subject;
		return url;
	}
}
