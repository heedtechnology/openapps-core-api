package org.heed.openapps.platform;

public interface ServiceListener {

	void fire(String action, String msg);
	
}
