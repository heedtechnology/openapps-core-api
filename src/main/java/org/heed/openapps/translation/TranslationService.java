package org.heed.openapps.translation;

public interface TranslationService {

	String translate(String in);
	
}
