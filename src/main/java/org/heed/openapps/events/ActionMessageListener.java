package org.heed.openapps.events;

import org.heed.openapps.data.ActionMessage;

public interface ActionMessageListener {

	void onMessage(ActionMessage msg);
	
}
