package org.heed.openapps.events;

public class Events {

	public static final int INIT = 1;
	public static final int START = 2;
	public static final int STARTED = 3;
	public static final int STOP = 4;
	public static final int STOPPED = 5;
	
	public static final int UI_OPEN = 10;
	public static final int UI_CLOSE = 11;
	
	public static final int UPDATE = 20;
	public static final int ADD_ENTITY = 21;
	public static final int UPDATE_ENTITY = 22;
	public static final int DELETE_ENTITY = 23;
	
	public static final int SEARCH = 30;
	public static final int RESULTS = 31;
	
}
