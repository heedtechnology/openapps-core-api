package org.heed.openapps.events;

public interface ShutdownListener {

	void shutdown();
	
}
