package org.heed.openapps.events;

public interface ActionListener {
	
	void fire(String action, Object obj);
		
}
