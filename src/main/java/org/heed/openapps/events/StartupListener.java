package org.heed.openapps.events;

public interface StartupListener {

	void startup();
	
}
