package org.heed.openapps.events;

public interface EventBus {

    /**
     * Subscribes the specified subscriber to the event bus.  A subscribed object
     * will be notified of any published events on the methods annotated with the
     * {@link EventHandler} annotation.
     * <p>
     * Each event handler method should take a single parameter indicating the
     * type of event it wishes to receive.  When events are published on the
     * bus, only subscribers who have an EventHandler method with a matching
     * parameter of the same type as the published event will receive the
     * event notification from the bus.
     * 
     * @param subscriber The object to subscribe to the event bus.
     */
    void subscribe(Object subscriber);
    
    
    /**
     * Removes the specified object from the event bus subscription list.  Once
     * removed, the specified object will no longer receive events posted to the
     * event bus.
     * 
     * @param subscriber The object previous subscribed to the event bus.
     */
    void unsubscribe(Object subscriber);
    
    
    /**
     * Sends a message on the bus which will be propagated to the appropriate
     * subscribers of the event type.  Only subscribers which have elected to
     * subscribe to the same event type as the supplied event will be notified
     * of the event.
     * <p>
     * Events can be vetoed, indicating that the event should not propagate to
     * the subscribers that don't have a veto.  The subscriber can veto by
     * setting the {@link EventHandler#canVeto()} return to true and by throwing
     * a {@link VetoException}.
     * <p>
     * There is no specification given as to how the messages will be delivered,
     * in terms of synchronous or asynchronous.  The only requirement is that
     * all the event handlers that can issue vetos be called before non-vetoing
     * handlers.  Most implementations will likely deliver messages asynchronously.
     * 
     * @param event The event to send out to the subscribers of the same type.
     */
    void publish(Object event);
    
    
    /**
     * Indicates whether the bus has pending events to publish.  Since message/event
     * delivery can be asynchronous (on other threads), the method can be used to
     * start or stop certain actions based on all the events having been published.
     * I.e. perhaps before an application closes, etc. 
     * 
     * @return True if events are still being delivered.
     */
    boolean hasPendingEvents();

}