package org.heed.openapps.web;

import org.heed.openapps.net.http.HttpRequest;

public interface DeviceDescriptionService {

	DeviceDescription getDeviceDescription(HttpRequest request);
	
}
