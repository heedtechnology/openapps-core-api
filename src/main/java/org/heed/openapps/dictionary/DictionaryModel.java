package org.heed.openapps.dictionary;

import org.heed.openapps.QName;

public class DictionaryModel {
	public static final String OPENAPPS_SYSTEM_NAMESPACE = "openapps_org_dictionary_1_0";
	
	public static final QName DICTIONARY = new QName(OPENAPPS_SYSTEM_NAMESPACE, "dictionary");
	public static final QName MODELS = new QName(OPENAPPS_SYSTEM_NAMESPACE, "models");
	public static final QName MODEL = new QName(OPENAPPS_SYSTEM_NAMESPACE, "model");
	public static final QName MODEL_FIELDS = new QName(OPENAPPS_SYSTEM_NAMESPACE, "fields");
	public static final QName MODEL_FIELD = new QName(OPENAPPS_SYSTEM_NAMESPACE, "field");
	public static final QName MODEL_RELATIONS = new QName(OPENAPPS_SYSTEM_NAMESPACE, "relations");
	public static final QName MODEL_RELATION = new QName(OPENAPPS_SYSTEM_NAMESPACE, "relation");
	public static final QName MODEL_FIELD_VALUES = new QName(OPENAPPS_SYSTEM_NAMESPACE, "values");
	public static final QName MODEL_FIELD_VALUE = new QName(OPENAPPS_SYSTEM_NAMESPACE, "value");
	public static final QName MODEL_ASPECTS = new QName(OPENAPPS_SYSTEM_NAMESPACE, "aspects");
	public static final QName MODEL_ASPECT = new QName(OPENAPPS_SYSTEM_NAMESPACE, "aspect");
	
	public static final QName QUALIFIED_NAME = new QName(OPENAPPS_SYSTEM_NAMESPACE, "qualified_name");
	public static final QName INHERITANCE = new QName(OPENAPPS_SYSTEM_NAMESPACE, "inheritance");
}
