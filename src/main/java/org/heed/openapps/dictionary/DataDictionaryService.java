package org.heed.openapps.dictionary;

import java.io.Serializable;
import java.util.List;

import org.heed.openapps.QName;


public interface DataDictionaryService extends Serializable {

	DataDictionary getSystemDictionary();
	List<DataDictionary> getBaseDictionaries();
	DataDictionary getDataDictionary(long id);
		
	Model getModel(QName qname);
	Model getModel(QName association, QName startNode, QName endNode);
	//void addUpdate(ModelObject modelObject);
	//void remove(long modelObjectId) throws DataDictionaryException;
	
	//DataDictionary reloadDataDictionary(long id) throws DataDictionaryException;
	
}
