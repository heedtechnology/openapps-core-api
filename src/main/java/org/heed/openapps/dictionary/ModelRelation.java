package org.heed.openapps.dictionary;

import java.util.ArrayList;
import java.util.List;

import org.heed.openapps.QName;
import org.heed.openapps.util.StringFormatUtility;

import com.fasterxml.jackson.annotation.JsonIgnore;


public class ModelRelation implements ModelObject {
	private static final long serialVersionUID = -1070736902043902312L;
	private Long id;
	private String uid;
	private QName qname;
	private String name;
	private String description;
	//private Long startId;
	private QName startName;
	//private Long endId;
	private QName endName;
	private int direction;
	private boolean many;
	private boolean cascade;
	private List<ModelField> fields = new ArrayList<ModelField>();
	private Model model;
	
	public static final int DIRECTION_OUTGOING = 1;
	public static final int DIRECTION_INCOMING = 2;
	
	public ModelRelation(){}
	public ModelRelation(Long id, QName qname) {
		this.id = id;
    	this.qname = qname;
	}
	public ModelRelation(Long id, QName startNode, QName endNode, int direction, QName qname) {
		this.id = id;
		this.startName = startNode;
		this.endName = endNode;
		this.direction = direction;
    	if(qname != null) {
    		this.qname = qname;
    	}
	}
	
	public Long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public QName getQName() {
		return qname;
	}
	public void setQName(QName qname) {
		this.qname = qname;
	}
	public int getDirection() {
		return direction;
	}
	public void setDirection(int direction) {
		this.direction = direction;
	}
	public boolean isMany() {
		return many;
	}
	public void setMany(boolean many) {
		this.many = many;
	}
	public boolean isCascade() {
		return cascade;
	}
	public void setCascade(boolean cascade) {
		this.cascade = cascade;
	}
	public List<ModelField> getFields() {
		return fields;
	}
	public void setFields(List<ModelField> fields) {
		this.fields = fields;
	}
	public QName getStartName() {
		return startName;
	}
	public void setStartName(QName startName) {
		this.startName = startName;
	}
	public QName getEndName() {
		return endName;
	}
	public void setEndName(QName endName) {
		this.endName = endName;
	}
	@JsonIgnore
	public Model getModel() {
		return model;
	}
	public void setModel(Model model) {
		this.model = model;
	}
	public String getName() {
		if(name == null && qname != null) return StringFormatUtility.toTitleCase(qname.getLocalName());
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
		
}