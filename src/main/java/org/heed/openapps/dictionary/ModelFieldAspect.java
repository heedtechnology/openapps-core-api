package org.heed.openapps.dictionary;

import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.heed.openapps.QName;


public class ModelFieldAspect implements ModelObject {
	private static final long serialVersionUID = 7314936515487882417L;
	private long id;
	private String uid;
	private Long fieldId;
	private QName qname;
	private String name;
	private String description;
	private List<ModelField> fields = new ArrayList<ModelField>();
	
	
	public ModelFieldAspect() {}
	public ModelFieldAspect(Long id, QName qname) {
		this.id = id;
		this.qname = qname;
	}	
	
	public Long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public Long getFieldId() {
		return fieldId;
	}
	public void setFieldId(Long fieldId) {
		this.fieldId = fieldId;
	}
	public QName getQName() {
		return qname;
	}
	public void setQName(QName qname) {
		this.qname = qname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<ModelField> getFields() {
		return fields;
	}
	public void setFields(List<ModelField> fields) {
		this.fields = fields;
	}
	
	public void fromJson(JsonObject object) {
    	if(object.containsKey("id")) setId(object.getJsonNumber("id").longValue());
    	if(object.containsKey("name")) setName(object.getString("name"));
	}
	public JsonObject toJsonObject() {
		JsonObjectBuilder agentBuilder = Json.createObjectBuilder();
		if(id > 0) agentBuilder.add("id", getId());
		agentBuilder.add("name", getName());
		if(description != null) agentBuilder.add("description", getDescription());
		return agentBuilder.build();
	}
}
