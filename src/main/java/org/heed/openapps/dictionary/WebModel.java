package org.heed.openapps.dictionary;

import org.heed.openapps.QName;

public class WebModel {
	public static final String WEB_MODEL_1_0_URI = "openapps_org_web_1_0";
	
	public static final QName DOCUMENT = new QName(WEB_MODEL_1_0_URI, "document");
	
}
