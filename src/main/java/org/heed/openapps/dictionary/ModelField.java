package org.heed.openapps.dictionary;

import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import org.heed.openapps.QName;
import org.heed.openapps.util.StringFormatUtility;

import com.fasterxml.jackson.annotation.JsonIgnore;


public class ModelField implements ModelObject {
	private static final long serialVersionUID = 4234751793033801218L;
	private long id;
	private String uid;
	private Model model;
	private QName qname;
	private String name;
	private String description;
	private String label;
	private String value;
	private String type;
	private int index;
	private boolean mandatory;
	private boolean unique;
	private boolean hidden;	
	private boolean sortable;
	private boolean searchable;
	private int minValue;
	private int maxValue;
	private int minSize;
	private int maxSize;
	private int order;
	private int format;
	private int sort;
	private String defaultValue;
	private List<ModelFieldValue> values = new ArrayList<ModelFieldValue>();
	private List<ModelFieldAspect> aspects = new ArrayList<ModelFieldAspect>();
	
	public static final String TYPE_NULL = "null";
    public static final String TYPE_BOOLEAN = "boolean";
    public static final String TYPE_INTEGER = "integer";
    public static final String TYPE_LONG = "long";
    public static final String TYPE_DOUBLE = "double";    
    public static final String TYPE_DATE = "date";
    public static final String TYPE_LONGTEXT = "longtext";
    public static final String TYPE_MEDIUMTEXT = "mediumtext";
    public static final String TYPE_SMALLTEXT = "smalltext";
    public static final String TYPE_SERIALIZABLE = "serializable";
    public static final String TYPE_ASPECT = "aspect";
    public static final String TYPE_VALUES = "values";
    public static final String TYPE_COMPUTED = "computed";
    
    public static final int FORMAT_PASSWORD = 1;
    public static final int FORMAT_EMAIL = 2;
    public static final int FORMAT_HTML = 3;
    public static final int FORMAT_URL = 4;
    public static final int FORMAT_CURRENCY = 5;
    
    public ModelField(){}
    public ModelField(long id, QName qname) {
    	this.id = id;
    	this.qname = qname;
	}
    public ModelField(String type, String label, QName qname) {
    	this.type = type;
    	this.label = label;
    	this.qname = qname;
	}
    
    public ModelFieldValue getValue(String value) {
    	for(ModelFieldValue modelValue : values) {
    		if(modelValue.getValue().equals(value))
    			return modelValue;
    	}
    	return null;
    }
    
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public QName getQName() {
		return qname;
	}
	public void setQName(QName qname) {
		this.qname = qname;
	}	
	public boolean isMandatory() {
		return mandatory;
	}
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}
	public boolean isUnique() {
		return unique;
	}
	public boolean isHidden() {
		return hidden;
	}
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}
	public void setUnique(boolean unique) {
		this.unique = unique;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}		
	public boolean isSortable() {
		return sortable;
	}
	public void setSortable(boolean sortable) {
		this.sortable = sortable;
	}
	public boolean isSearchable() {
		return searchable;
	}
	public void setSearchable(boolean searchable) {
		this.searchable = searchable;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public int getMinSize() {
		return minSize;
	}
	public void setMinSize(int minSize) {
		this.minSize = minSize;
	}
	public int getMaxSize() {
		return maxSize;
	}
	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public int getFormat() {
		return format;
	}
	public void setFormat(int format) {
		this.format = format;
	}
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	public List<ModelFieldValue> getValues() {
		return values;
	}
	public void setValues(List<ModelFieldValue> values) {
		this.values = values;
	}
	public List<ModelFieldAspect> getAspects() {
		return aspects;
	}
	public void setAspects(List<ModelFieldAspect> aspects) {
		this.aspects = aspects;
	}
	@JsonIgnore
	public Model getModel() {
		return model;
	}
	public void setModel(Model model) {
		this.model = model;
	}
	public String getName() {
		if(name == null) return StringFormatUtility.toTitleCase(qname.getLocalName());
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public int getMinValue() {
		return minValue;
	}
	public void setMinValue(int minValue) {
		this.minValue = minValue;
	}
	public int getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
	}
	
	public void fromJson(JsonObject object) {
    	if(object.containsKey("id")) setId(object.getJsonNumber("id").longValue());
    	if(object.containsKey("type")) setType(object.getString("type"));
    	if(object.containsKey("name")) setName(object.getString("name"));
    	if(object.containsKey("label")) setLabel(object.getString("label"));
    	if(object.containsKey("value")) setValue(object.getString("value"));
    	if(object.containsKey("values")) {
			JsonArray seeds = object.getJsonArray("values");
			for(JsonValue seedValue : seeds) {
				ModelFieldValue value = new ModelFieldValue();
				value.fromJson((JsonObject)seedValue);
				getValues().add(value);
			}
		}
    	if(object.containsKey("aspects")) {
    		JsonArray aspects = object.getJsonArray("aspects");
			for(JsonValue aspectValue : aspects) {
				ModelFieldAspect value = new ModelFieldAspect();
				value.fromJson((JsonObject)aspectValue);
				getAspects().add(value);
			}
		}
	}
	public JsonObject toJsonObject() {
		JsonObjectBuilder agentBuilder = Json.createObjectBuilder();
		if(id > 0) agentBuilder.add("id", getId());
		if(getType() != null) agentBuilder.add("type", getType());
		agentBuilder.add("name", getName());
		if(description != null) agentBuilder.add("description", getDescription());
		if(label != null) agentBuilder.add("label", getLabel());
		if(value != null) agentBuilder.add("value", getValue());
		JsonArrayBuilder fieldBuilder = Json.createArrayBuilder();
		for(ModelFieldValue value : getValues()) {
			fieldBuilder.add(value.toJsonObject());
		}
		agentBuilder.add("values", fieldBuilder);
		
		JsonArrayBuilder aspectBuilder = Json.createArrayBuilder();
		for(ModelFieldAspect value : getAspects()) {
			aspectBuilder.add(value.toJsonObject());
		}
		agentBuilder.add("aspects", aspectBuilder);
		
		return agentBuilder.build();
	}
}
