package org.heed.openapps.desktop;

public interface InitializatonListener {

	void initializationProgress(String message, int percent);
	void initializationCompleted();
	
}