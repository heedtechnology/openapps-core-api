package org.heed.openapps.net.http;

public interface DownloadListener {
	
	void update(int current, int total);
	
}
