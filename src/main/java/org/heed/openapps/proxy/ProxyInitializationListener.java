package org.heed.openapps.proxy;

public interface ProxyInitializationListener {

	void initializationCompleted();
	void initializationProgress(String message, int percent);
	
}
