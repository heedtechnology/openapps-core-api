/*
 * Copyright (C) 2009 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.heed.openapps.entity;

import java.text.SimpleDateFormat;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.heed.openapps.QName;
import org.heed.openapps.dictionary.ModelField;


public class Property implements java.io.Serializable {
	private static final long serialVersionUID = 2824085052507363147L;
	private Long id;
	private QName qname;
    private String type;
    private Object value;
        
    public static final String TYPE_NULL = "null";
    public static final String TYPE_BOOLEAN = "boolean";
    public static final String TYPE_INTEGER = "integer";
    public static final String TYPE_LONG = "long";
    public static final String TYPE_DOUBLE = "double";    
    public static final String TYPE_DATE = "date";
    public static final String TYPE_LONGTEXT = "longtext";
    public static final String TYPE_MEDIUMTEXT = "mediumtext";
    public static final String TYPE_SMALLTEXT = "smalltext";
    public static final String TYPE_SERIALIZABLE = "serializable";
    public static final String TYPE_ASPECT = "aspect";
    public static final String TYPE_VALUES = "values";
    public static final String TYPE_COMPUTED = "computed";
    
    
    public Property(QName qname, String type, Object value) {
    	this.qname = qname;
    	this.type = type;
		setValue(value);
	}
    public Property(JsonObject object) {
		if(object.containsKey("id")) setId(object.getJsonNumber("id").longValue());
		setQname(new QName(object.getString("qname")));
		if(object.containsKey("type")) setType(object.getJsonString("type").getString());
		if(object.containsKey("value")) {
			if(type == ModelField.TYPE_LONG)
				setValue(object.getJsonNumber("value").longValue());
			else if(type.equals(ModelField.TYPE_INTEGER))
				setValue(object.getJsonNumber("value").intValue());
			else if(type.equals(ModelField.TYPE_DOUBLE))
				setValue(object.getJsonNumber("value").doubleValue());
			else if(type.equals(ModelField.TYPE_BOOLEAN))
				setValue(object.getBoolean("value"));
			else if(type.equals(ModelField.TYPE_DATE)) {
				SimpleDateFormat sdf = new SimpleDateFormat();
				try {
					setValue(sdf.parse(object.getString("value")));
				} catch(Exception e) {
					setValue(object.getString("value"));
				}
			} else
				setValue(object.getString("value"));
		}
	}
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public QName getQName() {
        return qname;
    }
    
    public void setQname(QName qname) {
    	this.qname = qname;
    }
        
    public String toString() {
    	return String.valueOf(value);
    }    
    	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Object getValue() {
    	return value;
    }
    public void setValue(String type, Object value) {
    	setType(type);
    	this.value = String.valueOf(value);
    }
	public void setValue(Object value) {
		this.value = value;
		/*
    	if(value == null) {
    		setType(NULL);
    	} else if(DataTypeUtility.isBoolean(value)) {
    		setType(BOOLEAN);
    		this.value = String.valueOf(value);
    	} else if(DataTypeUtility.isInteger(value)) {
    		setType(INTEGER);
    		this.value = String.valueOf(value);
    	} else if(DataTypeUtility.isLong(value)) {
    		setType(LONG);
    		this.value = String.valueOf(value);
    	} else if(DataTypeUtility.isDouble(value)) {
        	setType(DOUBLE);
        	this.value = String.valueOf(value);
    	} else if(DataTypeUtility.isDate(value)) {
        	setType(DATE);
        	if(value instanceof String) this.value = ((String)value);
        	else {
        		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        		this.value = dateFormatter.format((Date)value);
        	}
        } else if(value instanceof String) {
        	String val = (String)value;
        	if(val.length() > 1024) {
        		setType(LONGTEXT);
        	} else {
        		setType(STRING);
        	}
        	this.value = ((String)value);
        }
        //else if(value instanceof ContentData) return CONTENT;
        else {
            // type is not recognised as belonging to any particular slot
        	setType(SERIALIZABLE);
        	this.value = new String((byte[])value);
        }
        */
    }
	
	public JsonObject toJsonObject() {
		JsonObjectBuilder builder = Json.createObjectBuilder();		
		if(getId() != null) builder.add("id", getId());
		if(getQName() != null) builder.add("qname", getQName().toString());
		builder.add("type", getType());
		if(getValue() != null) builder.add("value", String.valueOf(getValue()));
		return builder.build();
	}
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if (obj instanceof Property){
            Property that = (Property) obj;
            if(this.value.equals(that.getValue())) return true;
        }
        return false;
    }
}
