package org.heed.openapps.entity;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.heed.openapps.QName;
import org.heed.openapps.entity.data.FormatInstructions;


public interface EntityService extends Serializable {
	
	int count(QName qname);	
	
	EntityResultSet getEntities(QName qname, int page, int size);
	EntityResultSet getEntities(QName qname, QName propertyQname, int page, int size) throws InvalidEntityException;
	EntityResultSet getEntities(QName qname, QName propertyQname, Object value, int page, int size) throws InvalidEntityException;
	
	Entity getEntity(long id) throws InvalidEntityException;
	Entity getEntity(long id, boolean sources, boolean targets) throws InvalidEntityException;
	Entity getEntity(QName qname, long id) throws InvalidEntityException;
	Entity getEntity(String uid) throws InvalidEntityException;
	Entity getEntity(QName qname, QName propertyQname, Object value) throws InvalidEntityException;
		
	Long addEntity(Entity entity) throws InvalidEntityException;
	Long addEntity(Long source, Long target, QName association, List<Property> associstionProperties, Entity entity) throws InvalidEntityException;
	void addEntities(Collection<Entity> entities) throws InvalidEntityException;
	
	void updateEntity(Entity entity) throws InvalidEntityException;
	void removeEntity(QName entityQname, long id) throws InvalidEntityException;
	
	Object export(FormatInstructions instructions, Entity entity) throws InvalidEntityException;
	Object export(FormatInstructions instructions, Association association) throws InvalidEntityException;
	
	void cascadeProperty(QName qname, Long entityId, QName association, QName propertyName, Serializable propertyValue) throws InvalidEntityException, InvalidPropertyException;
	
	Association getAssociation(long id) throws InvalidAssociationException;
	Association getAssociation(QName qname, long source, long target) throws InvalidAssociationException;
	//Association getAssociation(HttpServletRequest request, QName associationQname) throws InvalidAssociationException;
	
	Long addAssociation(Association association) throws InvalidAssociationException;
	void updateAssociation(Association association) throws InvalidAssociationException;
	void removeAssociation(long id) throws InvalidAssociationException;		
	
	List<ImportProcessor> getImportProcessors(String name);
	ExportProcessor getExportProcessor(String name);
	EntityPersistenceListener getEntityPersistenceListener(String name);
	
	//ValidationResult validate(Entity entity) throws ModelValidationException;
	//ValidationResult validate(Association association) throws ModelValidationException;
	/*
	List<ImportProcessor> getImportProcessors(String name);
	void registerImportProcessor(String name, ImportProcessor processor);
	
	void registerExportProcessor(String name, ExportProcessor processor);
		
	void registerEntityPersistenceListener(String name, EntityPersistenceListener component);
	
	void startup();
	void shutdown();
	*/
}
