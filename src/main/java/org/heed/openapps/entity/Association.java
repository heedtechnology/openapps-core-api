/*
 * Copyright (C) 2009 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.heed.openapps.entity;
import java.io.Serializable;
import java.util.List;

import javax.json.JsonObject;

import org.heed.openapps.QName;


public interface Association extends Serializable {
	
    public Long getId();
    public void setId(Long id);
    public long getDictionary();
	public void setDictionary(long dictionary);
    public String getUid();
	public void setUid(String uid);
	public QName getSourceName();
	public void setSourceName(QName sourceName);
	public QName getTargetName();
	public void setTargetName(QName targetName);
	public QName getQName();
	public void setQname(QName qname);
	public Long getSource();
	public void setSource(Long source);
	public Entity getSourceEntity();
	public void setSourceEntity(Entity sourceEntity);
	public Long getTarget();
	public void setTarget(Long target);
	public Entity getTargetEntity();
	public void setTargetEntity(Entity targetEntity);
	public List<Property> getProperties();
    public String getSourceUid();
	public void setSourceUid(String sourceUid);
	public String getTargetUid();
	public void setTargetUid(String targetUid);
	public void setProperties(List<Property> properties);
    public boolean cascades();
	public void setCascades(boolean cascades);
	public void addProperty(QName qname, String type, Object value) throws InvalidPropertyException;
    public Property getProperty(QName qname);
    public String getPropertyValue(QName qname);
    public boolean hasProperty(QName qname);
    public void removeProperty(QName qname);
    public QName getSortField();
	public void setSortField(QName sortField);
	public JsonObject toJsonObject();
	
}


