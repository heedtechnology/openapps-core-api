/*
 * Copyright (C) 2009 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.heed.openapps.entity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import org.heed.openapps.QName;
import org.heed.openapps.SystemModel;
import org.heed.openapps.dictionary.ModelField;
import org.heed.openapps.node.Node;


public class EntityImpl implements Entity {
	private static final long serialVersionUID = -5414261842849202782L;
	private Node node;
	private long dictionary; //transient
	private String source; //the id from third-party data provider
    private QName parentQName;
    private QName labelField = SystemModel.NAME;
    private float rawScore;
	private int normalizedScore;
	private List<QName> childQNames = new ArrayList<QName>(0);
    private List<ACE> entries = new ArrayList<ACE>(0);
    private List<QName> aspects = new ArrayList<QName>(0); 
    private List<Property> properties = new ArrayList<Property>(0);
    private List<Association> sourceAssociations = new ArrayList<Association>();
    private List<Association> targetAssociations = new ArrayList<Association>();
    
    public EntityImpl() {
    	this.node = new Node();
    }
    public EntityImpl(Node node) {
    	this.node = node;
    }   
    public EntityImpl(QName qname) {
    	this.node = new Node();
    	node.setQName(qname);
    }
    public EntityImpl(long id, QName qname) {
    	this.node = new Node();
    	node.setId(id);
    	node.setQName(qname);
    }
    public EntityImpl(JsonObject json) {
    	fromJsonObject(json);
    }
    
    public Node getNode() {
		return node;
	}
	public void setNode(Node node) {
		this.node = node;
	}
	public long getDictionary() {
		return dictionary;
	}
	public void setDictionary(long dictionary) {
		this.dictionary = dictionary;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getLabel() {
    	if(hasProperty(labelField)) {
    		return getPropertyValue(labelField);
    	}
    	return "";
    }
    public QName getLabelField() {
		return labelField;
	}
	public void setLabelField(QName labelField) {
		this.labelField = labelField;
	}
	public float getRawScore() {
		return rawScore;
	}
	public void setRawScore(float score) {
		this.rawScore = score;
	}
	public int getNormalizedScore() {
		return normalizedScore;
	}
	public void setNormalizedScore(int normalizedScore) {
		this.normalizedScore = normalizedScore;
	}
	public List<ACE> getEntries() {
		return entries;
	}
	public void setEntries(List<ACE> entries) {
		this.entries = entries;
	}
	public void addAspect(QName qname) {
    	aspects.add(qname);
    }    
    public List<QName> getAspects() {
        return this.aspects;
    }
    public void setAspects(List<QName> aspects) {
        this.aspects = aspects;
    }    
    public List<Property> getProperties() {
        return this.properties;
    }
    public void setProperties(List<Property> properties) {
        this.properties = properties;
    }
    public void addProperty(QName qname, Object value)  throws InvalidPropertyException {
    	if(qname == null) throw new InvalidPropertyException("");
    	Property prop = getProperty(qname);
    	if(prop != null) {
    		prop.setValue(value);
    		return;
    	}
    	prop = new Property(qname, "0", value);
    	if(prop != null) properties.add(prop);
    }
    public void addProperty(String type, QName qname, Object value)  throws InvalidPropertyException {
    	if(qname == null) throw new InvalidPropertyException("");
    	Property prop = getProperty(qname);    	
    	if(prop != null) {
    		prop.setType(type);
    		prop.setValue(value);
    		return;
    	}
    	prop = new Property(qname, "0", value);
    	if(prop != null) {
    		prop.setType(type);
    		properties.add(prop);
    	}
    }
    public void addProperty(String localname, Object value)  throws InvalidPropertyException {
    	if(localname.contains(node.getQName().getNamespace())) addProperty(new QName(localname), value);
    	else addProperty(new QName(node.getQName().getNamespace(), localname), value);
    }
    public Property getProperty(String qname) {
    	try {
    		QName q = QName.createQualifiedName(qname);
    		return getProperty(q);
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    	return null;
    }
    public Property getProperty(QName qname) {
    	if(qname != null && qname.getNamespace() != null && qname.getLocalName() != null) {
    		for(Property prop : this.getProperties()) {
    			if(prop.getQName() != null && prop.getQName().getNamespace() != null && prop.getQName().getLocalName() != null &&
    					prop.getQName().getNamespace().equals(qname.getNamespace()) && prop.getQName().getLocalName().equals(qname.getLocalName())) 
    				return prop;
    		}
    	}
    	return null;
    }
    public String getPropertyValue(QName qname) {
    	Property prop = getProperty(qname.getNamespace(), qname.getLocalName());
    	if(prop != null && prop.getValue() != null) return prop.toString();
    	return null;
    }
    public boolean getBooleanPropertyValue(QName qname) {
		Property property =  getProperty(qname);
		if(property != null && property.getType().equals(ModelField.TYPE_BOOLEAN)) {
			if(property.getValue() instanceof String) 
				return Boolean.valueOf((String)property.getValue());
			return (Boolean)property.getValue();
		}
		return false;
	}
    public Property getProperty(String namespace, String localname) {
    	return getProperty(new QName(namespace, localname));
    }
    public List<Property> getProperties(QName qname) {
    	List<Property> props = new ArrayList<Property>();
    	if(qname != null && qname.getNamespace() != null && qname.getLocalName() != null) {
    		for(Property prop : this.getProperties()) {
    			if(prop.getQName().getNamespace().equals(qname.getNamespace()) && prop.getQName().getLocalName().equals(qname.getLocalName())) 
    				props.add(prop);
    		}
    	}
    	return props;
    }
    public boolean hasProperty(String qname) {
    	try {
    		QName q = QName.createQualifiedName(qname);
    		return hasProperty(q);
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    	return false;
    }
    public boolean hasProperty(QName qname) {
    	for(Property prop : this.getProperties()) {
    		if(prop.getQName().equals(qname) && prop.getValue() != null)
    			return true;
    	}
    	return false;
    }
    public void removeProperty(QName qname) {
    	List<Property> props = new ArrayList<Property>();
    	for(Property prop : this.getProperties()) {
    		if(prop.getQName().equals(qname)) {
    			props.add(prop);
    		}
    	}
    	for(Property prop : props) this.getProperties().remove(prop);
    }
    public List<Association> getAssociations() {
    	List<Association> assocs = new ArrayList<Association>();
    	assocs.addAll(sourceAssociations);
    	assocs.addAll(targetAssociations);
    	return assocs;
    }
    public List<Association> getAssociations(QName qname) {
    	List<Association> assocs = new ArrayList<Association>();
    	for(Association a : sourceAssociations) {
    		if(a.getQName().equals(qname)) assocs.add(a);
    	}
    	for(Association a : targetAssociations) {
    		if(a.getQName().equals(qname)) assocs.add(a);
    	}
    	return assocs;
    }
    public List<Association> getSourceAssociations() {
        return this.sourceAssociations;
    }
    public List<QName> getSourceAssociationQNames() {
    	List<QName> names = new ArrayList<QName>();
    	for(Association assoc : sourceAssociations) {
    		if(!names.contains(assoc.getQName())) names.add(assoc.getQName());
    	}
    	return names;
    }
    public Association getSourceAssociation(QName... qname) {
    	for(Association a : sourceAssociations) {
    		for(int i=0; i < qname.length; i++) {
    			if(a.getQName().equals(qname[i])) return a;
    		} 
    	}
    	return null;
    }
    public boolean containsSourceAssociation(QName qname) {
    	for(Association a : sourceAssociations) {
    		if(a.getQName().equals(qname)) return true;
    	}
    	return false;
    }
    public List<Association> getSourceAssociations(QName... qname) {
    	List<Association> assocs = new ArrayList<Association>();
    	for(Association a : sourceAssociations) {
    		for(int i=0; i < qname.length; i++) {
    			if(a.getQName().equals(qname[i])) assocs.add(a);
    		}    		
    	}
    	return assocs;
    }
    public List<Association> clearSourceAssociations(QName qname) {
    	return sourceAssociations;
    }
    public void setSourceAssociations(List<Association> sourceAssociations) {
        this.sourceAssociations = sourceAssociations;
    }
    
    public List<Association> getTargetAssociations() {
        return this.targetAssociations;
    }
    public List<QName> getTargetAssociationQNames() {
    	List<QName> names = new ArrayList<QName>();
    	for(Association assoc : targetAssociations) {
    		if(!names.contains(assoc.getQName())) names.add(assoc.getQName());
    	}
    	return names;
    }
    public Association getTargetAssociation(QName... qname) {
    	for(Association a : targetAssociations) {
    		for(int i=0; i < qname.length; i++) {
    			if(a.getQName().equals(qname[i])) return a;
    		}  
    	}
    	return null;
    }
    public boolean containsTargetAssociation(QName qname) {
    	for(Association a : targetAssociations) {
    		if(a.getQName().equals(qname)) return true;
    	}
    	return false;
    }
    public List<Association> getTargetAssociations(QName... qname) {
    	List<Association> assocs = new ArrayList<Association>();
    	for(Association a : targetAssociations) {
    		for(int i=0; i < qname.length; i++) {
    			if(a.getQName().equals(qname[i])) assocs.add(a);
    		}    		
    	}
    	return assocs;
    }
    public void setTargetAssociations(List<Association> targetAssociations) {
        this.targetAssociations = targetAssociations;
    }
   
    public Association getParent() {
    	if(this.parentQName != null) {
    		return getTargetAssociation(this.parentQName);
    	} else if(targetAssociations.size() > 0) return targetAssociations.get(0);
        return null;
    }
    public List<Association> getChildren() {
    	if(this.childQNames.size() > 0) {
    		return getSourceAssociations((QName[])this.childQNames.toArray());
    	} else return getSourceAssociations();
    }    
    
    public Long getId() {
        return node.getId();
    }    
    public void setId(Long id) {
        node.setId(id);
    } 
    public Long getXid() {
        return node.getXid();
    }    
    public void setXid(Long id) {
        node.setXid(id);
    } 
    public QName getQName() {
    	return node.getQName();
    }    
    public void setQName(QName qname) {
    	node.setQName(qname);
    }    
    public String getUid() {
        return node.getUid();
    }
    public void setUid(String uuid) {
        node.setUid(uuid);
    }
    public String getName() {
    	return node.getName();
    }
    public void setName(String name) {
    	node.setName(name);
    }
	public long getUser() {
		return node.getUser();
	}
	public void setUser(long user) {
		node.setUser(user);
	}
	public long getCreator() {
		return node.getCreator();
	}
	public void setCreator(long creator) {
		node.setCreator(creator);
	}	
	public long getModifier() {
		return node.getModifier();
	}
	public void setModifier(long modifier) {
		node.setModifier(modifier);
	}
	public long getCreated() {
		return node.getCreated();
	}
	public void setCreated(long created) {
		node.setCreated(created);
	}
	public long getModified() {
		return node.getModified();
	}
	public void setModified(long modified) {
		node.setModified(modified);
	}
	public long getAccessed() {
		return node.getAccessed();
	}
	public void setAccessed(long accessed) {
		node.setAccessed(accessed);
	}
	public Boolean getDeleted() {
		return node.isDeleted();
	}
	public void setDeleted(Boolean deleted) {
		node.setDeleted(deleted);
	}
	
	public QName getParentQName() {
		return parentQName;
	}
	public void setParentQName(QName parentQName) {
		this.parentQName = parentQName;
	}
	public List<QName> getChildQNames() {
		return childQNames;
	}
	public void setChildQNames(List<QName> childQNames) {
		this.childQNames = childQNames;
	}	
	public Map<String,Object> getPropertyMap() {
		Map<String,Object> map = new HashMap<String,Object>();
		for(Property prop : properties) {
			map.put(prop.getQName().getLocalName(), prop.toString());
		}
		return map;
	}
	public Map<String,String> toMap() {
		Map<String,String> map = new HashMap<String,String>();
		map.put("namespace", node.getQName().getNamespace());
		map.put("localname", node.getQName().getLocalName());
		for(Property property : properties) {
			map.put(property.getQName().getLocalName(), property.toString());
		}
		return map;
	}
	public void fromJsonObject(JsonObject object) {
    	if(object.containsKey("id")) setId(object.getJsonNumber("id").longValue());
		if(object.containsKey("uid")) setUid(object.getString("uid"));
		if(object.containsKey("xid")) setXid(object.getJsonNumber("xid").longValue());
		if(object.containsKey("name")) setName(object.getString("name"));
		if(object.containsKey("qname")) setQName(new QName(object.getString("qname")));
		if(object.containsKey("created")) setCreated(object.getJsonNumber("created").longValue());
		if(object.containsKey("modified")) setModified(object.getJsonNumber("modified").longValue());
		if(object.containsKey("creator")) setCreator(object.getJsonNumber("creator").longValue());
		if(object.containsKey("modifier")) setModifier(object.getJsonNumber("modifier").longValue());
		if(object.containsKey("accessed")) setAccessed(object.getJsonNumber("accessed").longValue());
		if(object.containsKey("user")) setUser(object.getJsonNumber("user").longValue());
		if(object.containsKey("deleted")) setDeleted(object.getBoolean("deleted"));
		if(object.containsKey("properties")) {
			JsonArray propertyValues = object.getJsonArray("properties");
			for(JsonValue propertyValue : propertyValues) {
				Property property = new Property((JsonObject)propertyValue);
				properties.add(property);
			}
		}
		if(object.containsKey("source_associations")) {
			JsonArray attributes = object.getJsonArray("source_associations");
			for(JsonValue attributeValue : attributes) {
				Association association = new AssociationImpl((JsonObject)attributeValue);
				getSourceAssociations().add(association);
			}
		}
		if(object.containsKey("target_associations")) {
			JsonArray attributes = object.getJsonArray("target_associations");
			for(JsonValue attributeValue : attributes) {
				Association association = new AssociationImpl((JsonObject)attributeValue);
				getTargetAssociations().add(association);
			}
		}
    }
	public JsonObject toSimpleJsonObject() {
		JsonObjectBuilder builder = Json.createObjectBuilder();
		if(getId() != null) builder.add("id", getId());
		if(getUid() != null) builder.add("uid", getUid());
		if(getXid() != null) builder.add("xid", getXid());
		if(getName() != null) builder.add("name", getName());
		if(getQName() != null) builder.add("qname", getQName().toString());
		builder.add("created", getCreated());
		builder.add("modified", getModified());
		builder.add("creator", getCreator());
		builder.add("modifier", getModifier());
		builder.add("accessed", getAccessed());
		builder.add("user", getUser());
		builder.add("deleted", getDeleted());
		for(Property property : properties) {
			builder.add(property.getQName().getLocalName(), property.toString());
		}
		
		return builder.build();
	}
	public JsonObject toJsonObject() {
		JsonObjectBuilder builder = Json.createObjectBuilder();		
		if(getId() != null) builder.add("id", getId());
		if(getUid() != null) builder.add("uid", getUid());
		if(getXid() != null) builder.add("xid", getXid());
		if(getName() != null) builder.add("name", getName());
		if(getQName() != null) builder.add("qname", getQName().toString());
		builder.add("created", getCreated());
		builder.add("modified", getModified());
		builder.add("creator", getCreator());
		builder.add("modifier", getModifier());
		builder.add("accessed", getAccessed());
		builder.add("user", getUser());
		builder.add("deleted", getDeleted());
		JsonArrayBuilder propertyBuilder = Json.createArrayBuilder();
		for(Property property : properties) {
			propertyBuilder.add(property.toJsonObject());
		}
		builder.add("properties", propertyBuilder);

		return builder.build();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((node == null) ? 0 : node.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntityImpl other = (EntityImpl) obj;
		if (node == null) {
			if (other.node != null)
				return false;
		} else if (!node.equals(other.node))
			return false;
		return true;
	}
	
}