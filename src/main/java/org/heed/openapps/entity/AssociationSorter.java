package org.heed.openapps.entity;
import java.io.Serializable;
import java.util.Comparator;

import org.heed.openapps.data.Sort;


public class AssociationSorter implements Comparator<Association>, Serializable {
	private static final long serialVersionUID = 3502967410907833006L;
	private EntitySorter sorter;
	
	
	public AssociationSorter(Sort sort) {
		this.sorter = new EntitySorter(sort);
	}
	
	@Override
	public int compare(Association assoc1, Association assoc2) {
		if(assoc1 == null || assoc1 == null) return 0;
		
		if(assoc1.getQName().equals(assoc2.getQName())) {			
			if(assoc1.getTargetEntity() != null && assoc2.getTargetEntity() != null) 
				return sorter.compare(assoc1.getTargetEntity(), assoc2.getTargetEntity());
			else if(assoc1.getSourceEntity() != null && assoc2.getSourceEntity() != null) 
				return sorter.compare(assoc1.getSourceEntity(), assoc2.getSourceEntity());
		}
		return 0;
	}
	
}
