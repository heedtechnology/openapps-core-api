/*
 * Copyright (C) 2009 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.heed.openapps.entity.data;


public class Column {
	private String from;
	private String to;
	private boolean duplicates = true;
	private boolean fixedValue = false;
	
	public Column(String from, String to) {
		this.from = from;
		this.to = to;
	}

	
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public boolean allowDuplicates() {
		return duplicates;
	}
	public void setDuplicates(boolean duplicates) {
		this.duplicates = duplicates;
	}
	public boolean isFixedValue() {
		return fixedValue;
	}
	public void setFixedValue(boolean fixedValue) {
		this.fixedValue = fixedValue;
	}
		
}
