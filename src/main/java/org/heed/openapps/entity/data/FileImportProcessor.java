/*
 * Copyright (C) 2009 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.heed.openapps.entity.data;

import java.io.InputStream;
import java.util.Map;

import org.heed.openapps.entity.ImportProcessor;


public abstract class FileImportProcessor extends ImportProcessor {
	private static final long serialVersionUID = 2280240342093932690L;
	private Long storeid;
	private Mapping mapping;
	
	
	public FileImportProcessor(){}
	public FileImportProcessor(String id, String name) {
		setId(id);
		setName(name);
	}
	
	public abstract void process(InputStream stream, Map<String, Object> properties) throws Exception;
	//public void process(InputStream stream, Map<String, Object> properties) throws Exception {
		//getEntities().clear();
	//}	
	
	public Mapping getMapping() {
		return mapping;
	}
	public void setMapping(Mapping mapping) {
		this.mapping = mapping;
	}
	public void setMappingFile(Mapping mapping) {
		this.mapping = mapping;
	}
	public Long getStoreid() {
		return storeid;
	}
	public void setStoreid(Long storeid) {
		this.storeid = storeid;
	}
	
}
