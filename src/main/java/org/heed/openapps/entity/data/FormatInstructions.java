package org.heed.openapps.entity.data;

public class FormatInstructions {	
	private boolean printSources = false;
	private boolean printTargets = false;
	private boolean isTree = false;
	private boolean isPublic = false;
	private boolean hasChildren = true;
	private String format = FORMAT_JSON;
	private String view = "default";
	private int page = 1;
	
	public static final String FORMAT_XML = "xml";
	public static final String FORMAT_CSV = "csv";
	public static final String FORMAT_JSON = "json";
	
	public FormatInstructions() {}
	
	public FormatInstructions(boolean isTree) {
		this.isTree = isTree;
	}
	public FormatInstructions(boolean isTree, String format) {
		this.isTree = isTree;
		this.format = format;
	}
	public FormatInstructions(boolean isTree, boolean printSources, boolean printTargets) {
		this.isTree = isTree;
		this.printSources = printSources;
		this.printTargets = printTargets;
	}
	public FormatInstructions(boolean isPublic, boolean isTree, boolean printSources, boolean printTargets) {
		this.isPublic = isPublic;
		this.isTree = isTree;
		this.printSources = printSources;
		this.printTargets = printTargets;
	}
	
	public boolean isTree() {
		return isTree;
	}
	public void setTree(boolean isTree) {
		this.isTree = isTree;
	}
	public boolean printSources() {
		return printSources;
	}
	public void setPrintSources(boolean printSources) {
		this.printSources = printSources;
	}
	public boolean printTargets() {
		return printTargets;
	}
	public void setPrintTargets(boolean printTargets) {
		this.printTargets = printTargets;
	}
	public boolean isPublic() {
		return isPublic;
	}
	public boolean hasChildren() {
		return hasChildren;
	}
	public void setHasChildren(boolean hasChildren) {
		this.hasChildren = hasChildren;
	}
	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getView() {
		return view;
	}
	public void setView(String view) {
		this.view = view;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	
}
