package org.heed.openapps.entity;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Comparator;

import org.heed.openapps.SystemModel;
import org.heed.openapps.data.Sort;
import org.heed.openapps.dictionary.ModelField;


public class EntitySorter implements Comparator<Entity>, Serializable {
	private static final long serialVersionUID = 6893881851620745675L;
	private SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
	private Sort sort;
	
	
	public EntitySorter(Sort sort) {
		this.sort = sort;
	}
	
	@SuppressWarnings("deprecation")
	public int compare(Entity e1, Entity e2) {
		int response = 0;
		String field1 = null;
		String field2 = null;
		try {
			if(e1 == null || e1 == null) response = 0;
			Property property1 = null;
			Property property2 = null;
			if(sort.getField().equals(SystemModel.NAME.toString())) {
				property1 = sort.isReverse() ? new Property(SystemModel.NAME, "0", e2.getName()) : new Property(SystemModel.NAME, "0", e1.getName());
				property2 = sort.isReverse() ? new Property(SystemModel.NAME, "0", e1.getName()) : new Property(SystemModel.NAME, "0", e2.getName());
			} else {
				property1 = sort.isReverse() ? e2.getProperty(sort.getField()) : e1.getProperty(sort.getField());
				property2 = sort.isReverse() ? e1.getProperty(sort.getField()) : e2.getProperty(sort.getField());
			}
			if(property1 != null && property2 != null) {
				if(sort.getType() == null) sort.setType(property1.getType());
				field1 = removeTags(String.valueOf(property1));
				field2 = removeTags(String.valueOf(property2));
				if(sort.getType().equals(ModelField.TYPE_DATE)) {
					try {
						response = dateFormatter.parse(field1).compareTo(dateFormatter.parse(field2));
					} catch(Exception e) {}
				} else if(sort.getType().equals(ModelField.TYPE_INTEGER) || sort.getType().equals(Sort.DOUBLE) || sort.getType().equals(Sort.LONG)) {
					response = Integer.valueOf(field1).compareTo(Integer.valueOf(field2));
				} else {
					for(int i=0; i < field1.length(); i++) {
						if(field2.length() > i && field1.charAt(i) != field2.charAt(i)) {
							if(Character.isLetter(field1.charAt(i)) && Character.isLetter(field2.charAt(i)))
								response = Character.valueOf(field1.charAt(i)).compareTo(Character.valueOf(field2.charAt(i)));
							else if(Character.isSpace(field1.charAt(i)) && !Character.isSpace(field2.charAt(i)))
								response = -1;
							else if(Character.isSpace(field2.charAt(i)) && !Character.isSpace(field1.charAt(i)))
								response = 1;
							else if(Character.isDigit(field1.charAt(i)) && Character.isDigit(field2.charAt(i))) {
								StringBuffer digit1 = new StringBuffer();
								StringBuffer digit2 = new StringBuffer();
								int index = i;
								while(field1.length() > index && Character.isDigit(field1.charAt(index))) {
									digit1.append(field1.charAt(index));
									index++;
								}
								index = i;
								while(field2.length() > index && Character.isDigit(field2.charAt(index))) {
									digit2.append(field2.charAt(index));
									index++;
								}
								if(Integer.valueOf(digit1.toString()) > Integer.valueOf(digit2.toString()))
									response = 1;
								else response = -1;
							}
						}
					}
					response = field1.compareTo(field2);
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	protected String removeTags(String in) {
		if(in == null) return "";
    	return in.replaceAll("\\<.*?>","");
	}
}