/*
 * Copyright (C) 2009 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.heed.openapps.entity;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.json.JsonObject;

import org.heed.openapps.QName;
import org.heed.openapps.node.Node;


public interface Entity extends Serializable {
	
	public Node getNode();
	public void setNode(Node node);
	public long getDictionary();
	//public void setDictionary(long dictionary);
	public String getSource();
	public void setSource(String source);
	public String getLabel();
    public QName getLabelField();
	public void setLabelField(QName labelField);
	public List<ACE> getEntries();
	public void setEntries(List<ACE> entries);
	public void addAspect(QName qname);
    public List<QName> getAspects();
    public void setAspects(List<QName> aspects);
    public List<Property> getProperties();
    public void setProperties(List<Property> properties);
    public void addProperty(QName qname, Object value)  throws InvalidPropertyException;
    public void addProperty(String type, QName qname, Object value)  throws InvalidPropertyException;
    public void addProperty(String localname, Object value)  throws InvalidPropertyException;
    public Property getProperty(String qname);
    public Property getProperty(QName qname);
    public String getPropertyValue(QName qname);
    public boolean getBooleanPropertyValue(QName qname);
    public Property getProperty(String namespace, String localname);
    public List<Property> getProperties(QName qname);
    public boolean hasProperty(String qname);
    public boolean hasProperty(QName qname);
    public void removeProperty(QName qname);
    public List<Association> getAssociations();
    public List<Association> getAssociations(QName qname);
    public List<Association> getSourceAssociations();
    public List<QName> getSourceAssociationQNames();
    public Association getSourceAssociation(QName... qname);
    public boolean containsSourceAssociation(QName qname);
    public List<Association> getSourceAssociations(QName... qname);
    public List<Association> clearSourceAssociations(QName qname);
    public void setSourceAssociations(List<Association> sourceAssociations);
    
    public List<Association> getTargetAssociations();
    public List<QName> getTargetAssociationQNames();
    public Association getTargetAssociation(QName... qname);
    public boolean containsTargetAssociation(QName qname);
    public List<Association> getTargetAssociations(QName... qname);
    public void setTargetAssociations(List<Association> targetAssociations);
   
    public Association getParent();
    public List<Association> getChildren();    
    
    public Long getId();
    public void setId(Long id);
    public Long getXid();
    public void setXid(Long id);
    public QName getQName();
    public void setQName(QName qname);
    public String getUid();
    public void setUid(String uuid);
    public String getName();
    public void setName(String name);
	public long getUser();
	public void setUser(long user);
	public long getCreator();
	public void setCreator(long creator);
	public long getModifier();
	public void setModifier(long modifier);
	public long getCreated();
	public void setCreated(long created);
	public long getModified();
	public void setModified(long modified);
	public long getAccessed();
	public void setAccessed(long accessed);
	public Boolean getDeleted();
	public void setDeleted(Boolean deleted);
	public float getRawScore();
	public void setRawScore(float score);
	public int getNormalizedScore();
	public void setNormalizedScore(int normalizedScore);
	
	public QName getParentQName();
	public void setParentQName(QName parentQName);
	public List<QName> getChildQNames();
	public void setChildQNames(List<QName> childQNames);
	public Map<String,Object> getPropertyMap();
	public Map<String,String> toMap();
	public JsonObject toJsonObject();
	public void fromJsonObject(JsonObject object);
}