package org.heed.openapps.entity.indexing;

public class IndexField {
	private String type;
	private String name;
	private Object value;
	private boolean tokenized = true;
	
	public IndexField() {}
	public IndexField(String type, String name, Object value, boolean tokenized) {
		this.type = type;
		this.name = name;
		this.value = value;
		this.tokenized = tokenized;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public boolean isTokenized() {
		return tokenized;
	}
	public void setTokenized(boolean tokenized) {
		this.tokenized = tokenized;
	}
}
