package org.heed.openapps.entity.indexing;

import org.heed.openapps.QName;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.InvalidEntityException;

public interface EntityIndexer {

	void index(Entity entity, IndexEntity data) throws InvalidEntityException;
	void deindex(QName qname, Entity entity) throws InvalidEntityException;
	
}
