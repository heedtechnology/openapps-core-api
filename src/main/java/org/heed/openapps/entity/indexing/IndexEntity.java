package org.heed.openapps.entity.indexing;
import java.util.ArrayList;
import java.util.List;

import org.heed.openapps.QName;


public class IndexEntity {
	private long id;
	private String content;
	private boolean delete;
	private StringBuffer freeText = new StringBuffer();
	private List<QName> qnames = new ArrayList<QName>();
	private List<IndexField> fields = new ArrayList<IndexField>();
	private List<IndexFacet> facets = new ArrayList<IndexFacet>();
	
	public IndexEntity() {}
	public IndexEntity(long id) {
		this.id = id;
	}
	public IndexEntity(long id, List<QName> qnames, List<IndexField> fields) {
		this.id = id;
		this.qnames = qnames;
		this.fields = fields;
	}
	public IndexEntity(long id, List<QName> qnames, List<IndexField> fields, List<IndexFacet> facets) {
		this.id = id;
		this.qnames = qnames;
		this.fields = fields;
		this.facets = facets;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public List<IndexField> getFields() {
		return fields;
	}
	public void setFields(List<IndexField> fields) {
		this.fields = fields;
	}
	public List<IndexFacet> getFacets() {
		return facets;
	}
	public void setFacets(List<IndexFacet> facets) {
		this.facets = facets;
	}	
	public List<QName> getQnames() {
		return qnames;
	}
	public void setQnames(List<QName> qnames) {
		this.qnames = qnames;
	}
	public boolean isDelete() {
		return delete;
	}
	public void setDelete(boolean delete) {
		this.delete = delete;
	}
	public void appendFreeText(String text) {
		freeText.append(text);
	}
	public String getFreeText() {
		return freeText.toString().trim();
	}
}
