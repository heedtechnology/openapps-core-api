package org.heed.openapps.entity;
import java.util.List;

import org.heed.openapps.entity.indexing.IndexField;


public interface EntityPersistenceListener {

	void onBeforeAdd(Entity entity);
	void onAfterAdd(Entity entity);
	void onBeforeUpdate(Entity oldValue, Entity newValue);
	void onAfterUpdate(Entity entity);
	void onBeforeDelete(Entity entity);
	void onAfterDelete(Entity entity);
	
	void onBeforeAssociationAdd(Association assoc);
	void onAfterAssociationAdd(Association assoc);
	void onBeforeAssociationUpdate(Association assoc);
	void onAfterAssociationUpdate(Association assoc);
	void onBeforeAssociationDelete(Association assoc);
	void onAfterAssociationDelete(Association assoc);
	
	//Entity extractEntity(long node, QName entityQname);
	
	List<IndexField> index(Entity entity);
	
}
