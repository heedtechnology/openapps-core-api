package org.heed.openapps.search;
import java.util.HashMap;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import org.heed.openapps.QName;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityImpl;
import org.heed.openapps.entity.Property;


public class SearchResult {
	private float rawScore;
	private int normalizedScore;
	private String classification;
	private Entity entity;
	private Map<String,String> data = new HashMap<String,String>();
	
	
	public SearchResult(Entity entity) {
		this.entity = entity;
	}
	public SearchResult(JsonObject obj) {
		fromJson(obj);
	}
	
	public long getId() {
		return entity.getId();
	}
	public String getUid() {
		return entity.getUid();
	}
	public QName getQName() {
		return entity.getQName();
	}
	public long getCreated() {
		return entity.getCreated();
	}
	public boolean hasProperty(QName qname) {
		return entity.hasProperty(qname);
	}
	public Property getProperty(QName qname) {
		return entity.getProperty(qname);		
	}
	
	public String get(String key) {
		return data.get(key);
	}
	public void put(String key, String value) {
		data.put(key, value);
	}
	public Map<String,String> getData() {
		return data;
	}
	
	public float getRawScore() {
		return rawScore;
	}
	public void setRawScore(float score) {
		this.rawScore = score;
	}
	public int getNormalizedScore() {
		return normalizedScore;
	}
	public void setNormalizedScore(int normalizedScore) {
		this.normalizedScore = normalizedScore;
	}
	public String getClassification() {
		return classification;
	}
	public void setClassification(String classification) {
		this.classification = classification;
	}
	public Entity getEntity() {
		return entity;
	}
	public void setEntity(Entity entity) {
		this.entity = entity;
	}
	
	public void fromJson(JsonObject object) {
		if(object.containsKey("classification")) setClassification(object.getString("classification")); 
		if(object.containsKey("rawScore")) setRawScore(object.getJsonNumber("rawScore").intValue());
    	if(object.containsKey("entity")) {
			Entity entity = new EntityImpl();
			JsonValue value = object.get("entity");
			entity.fromJsonObject((JsonObject)value);
			setEntity(entity);			
		}
    	if(object.containsKey("data")) {
    		JsonObject resultObjs = object.getJsonObject("data");
			for(String key : resultObjs.keySet()) {
				data.put(key, resultObjs.getString(key));				
			}			
		}
    }
	public JsonObject toJsonObject() {
		JsonObjectBuilder builder = Json.createObjectBuilder();	
		if(getClassification() != null) builder.add("classification", getClassification());
		builder.add("rawScore", getRawScore());
		builder.add("entity", getEntity().toJsonObject());
		
		/** Results **/
		JsonObjectBuilder resultBuilder = Json.createObjectBuilder();
		for(String key : data.keySet()) {
			String value = data.get(key);
			resultBuilder.add(key, value);
		}
		builder.add("data", resultBuilder.build());
		return builder.build();
	}
}
