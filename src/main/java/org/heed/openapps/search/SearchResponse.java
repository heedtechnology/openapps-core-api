package org.heed.openapps.search;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import org.heed.openapps.search.SearchNode;
import org.heed.openapps.search.Token;


public class SearchResponse {
	private String searchId;
	private int resultSize;
	private int startRow;
	private int endRow;	
	private List<SearchResult> results = new ArrayList<SearchResult>();
	private List<SearchAttribute> attributes = new ArrayList<SearchAttribute>();
	private List<SearchNode> breadcrumb;
	private List<Token> tokens;
	private BitSet bits;
	private float topScore;
	private String parse;
	private String explanation;
	private long time;
	
	
	public String getSearchId() {
		return searchId;
	}
	public void setSearchId(String searchId) {
		this.searchId = searchId;
	}
	public int getResultSize() {
		return resultSize;
	}
	public void setResultSize(int resultSize) {
		this.resultSize = resultSize;
	}
	public int getStartRow() {
		return startRow;
	}
	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}
	public int getEndRow() {
		return endRow;
	}
	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}	
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public String getParse() {
		return parse;
	}
	public void setParse(String parse) {
		this.parse = parse;
	}
	public String getExplanation() {
		return explanation;
	}
	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}
	public SearchResult getResult(int index) {
		return results.get(index);
	}
	public List<SearchResult> getResults() {
		return results;
	}
	public void setResults(List<SearchResult> results) {
		this.results = results;
	}
	public List<SearchAttribute> getAttributes() {
		return attributes;
	}
	public void setAttributes(List<SearchAttribute> attributes) {
		this.attributes = attributes;
	}
	public SearchAttribute findAttribute(String name) {
		for(int i=0; i < attributes.size(); i++) {
			if(attributes.get(i).getName().equals(name)) return attributes.get(i);
		}
		SearchAttribute att = new SearchAttribute(name);
		attributes.add(att);
		return att;
	}
	public List<SearchNode> getBreadcrumb() {
		return breadcrumb;
	}
	public void setBreadcrumb(List<SearchNode> breadcrumb) {
		this.breadcrumb = breadcrumb;
	}
	public List<Token> getTokens() {
		return tokens;
	}
	public void setTokens(List<Token> tokens) {
		this.tokens = tokens;
	}
	public BitSet getBits() {
		return bits;
	}
	public void setBits(BitSet bits) {
		this.bits = bits;
	}
	public float getTopScore() {
		return topScore;
	}
	public void setTopScore(float topScore) {
		this.topScore = topScore;
	}
	
	public void fromJsonObject(JsonObject object) {
		if(object.containsKey("startRow")) setStartRow(object.getJsonNumber("startRow").intValue());
    	if(object.containsKey("endRow")) setEndRow(object.getJsonNumber("endRow").intValue());    	
    	
    	if(object.containsKey("resultSize")) setResultSize(object.getJsonNumber("resultSize").intValue());
    	if(object.containsKey("topScore")) setTopScore(object.getJsonNumber("topScore").intValue());
    	if(object.containsKey("time")) setTime(object.getJsonNumber("time").longValue());
    	if(object.containsKey("results")) {
    		JsonArray resultsObjs = object.getJsonArray("results");
			for(JsonValue resultValue : resultsObjs) {
				SearchResult result = new SearchResult((JsonObject)resultValue);
				getResults().add(result);
			}
    	}
    	if(object.containsKey("attributes")) {
    		JsonArray attributesObjs = object.getJsonArray("attributes");
			for(JsonValue attributeValue : attributesObjs) {
				SearchAttribute attribute = new SearchAttribute((JsonObject)attributeValue);
				getAttributes().add(attribute);
			}
    	}
    	/*
    	 * 	private List<SearchNode> breadcrumb;
			private List<Token> tokens;
    	 */
    }
	public JsonObject toJsonObject() {
		JsonObjectBuilder builder = Json.createObjectBuilder();	
		builder.add("startRow", getStartRow());
		builder.add("endRow", getEndRow());
		
		
		/** Results **/
		JsonArrayBuilder resultBuilder = Json.createArrayBuilder();
		for(SearchResult result : results) {
			resultBuilder.add(result.toJsonObject());
		}
		builder.add("results", resultBuilder);
		return builder.build();
	}
}
