package org.heed.openapps.search;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonValue;

import org.heed.openapps.QName;
import org.heed.openapps.User;
import org.heed.openapps.data.Sort;
import org.heed.openapps.dictionary.ModelField;
import org.heed.openapps.entity.Property;
import org.heed.openapps.search.Clause;
import org.heed.openapps.search.Parameter;


public class SearchRequest {
	private String searcherId = "default";
	private long id;
	private long xid;
	private List<QName> qnames = new ArrayList<QName>();
	private String query;
	private String context = "";
	private int startRow = 0;
	private int endRow = 0;
	private String operator = "OR";
	private boolean deleted = false;
	private boolean attributes = true;
	private boolean sources = false;
	private boolean tokenized = true;
	private boolean caseSensitive = false;
	private boolean isPublic = true;
	private User user;
	private long dictionary;
	private String parse;
	private Map<String, String[]> requestParameters = new HashMap<String, String[]>();
	private String[] fields = new String[0];
	
	private List<Property> properties = new ArrayList<Property>();
	private List<Parameter> parameters = new ArrayList<Parameter>();
	private List<Clause> clauses = new ArrayList<Clause>();
	private List<Sort> sorts = new ArrayList<Sort>();
	private List<Long> groups = new ArrayList<Long>();
	
	
	private Object nativeQuery;
	
	public static final String OPERATOR_OR = "OR";
	public static final String OPERATOR_AND = "AND";
	
	public SearchRequest() {}
	public SearchRequest(QName qname) {
		this.qnames.add(qname);
		if(qname != null) searcherId = qname.toString();
	}
	public SearchRequest(QName qname, String query) {
		this.qnames.add(qname);
		this.query = query;
		if(qname != null) searcherId = qname.toString();
	}
	public SearchRequest(String query, QName... qnames) {
		for(QName q : qnames) {
			this.qnames.add(q);
		}
		this.query = query;
		//if(qname != null) searcherId = qname.toString();
	}
	public SearchRequest(QName entityQname, String query, String sort, boolean reverse) {
		this.qnames.add(entityQname);
		this.query = query;
		addSort(new Sort(ModelField.TYPE_SMALLTEXT, sort, reverse));
	}
	public SearchRequest(QName entityQname, String field, String queryString, String sort, boolean reverse) {
		this.qnames.add(entityQname);
		this.query = queryString;
		this.fields = new String[] {field};
		addSort(new Sort(ModelField.TYPE_SMALLTEXT, sort, reverse));
	}
	
	public String getSearcherId() {
		return searcherId;
	}
	public void setSearcherId(String searcherId) {
		this.searcherId = searcherId;
	}	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getXid() {
		return xid;
	}
	public void setXid(long xid) {
		this.xid = xid;
	}
	public QName getQname() {
		if(qnames.size() > 0) return qnames.get(0);
		return null;
	}
	public void setQname(QName qname) {
		this.qnames.add(qname);
	}
	public List<QName> getQnames() {
		return qnames;
	}
	public void setQnames(List<QName> qnames) {
		this.qnames = qnames;
	}
	public void setQnames(QName[] qnames) {
		for(QName qname : qnames) {
			if(!this.qnames.contains(qname))
				this.qnames.add(qname);
		}
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	public void addProperty(Property property) {
		this.properties.add(property);
	}
	public List<Property> getProperties() {
		return properties;
	}
	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}
	public void addParameter(Parameter p) {
		parameters.add(p);
	}
	public void addParameter(String name, String value) {
		parameters.add(new Parameter(name, value));
	}
	public List<Parameter> getParameters() {
		return parameters;
	}
	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}
	public void addClause(Clause clause) {
		clauses.add(clause);
	}
	public List<Clause> getClauses() {
		return clauses;
	}
	public void setClauses(List<Clause> clauses) {
		this.clauses = clauses;
	}
	public List<Sort> getSorts() {
		return sorts;
	}
	public void setSorts(List<Sort> sorts) {
		this.sorts = sorts;
	}
	public void addSort(Sort sort) {
		sorts.add(sort);
	}	
	public List<Long> getGroups() {
		return groups;
	}
	public void setGroups(List<Long> groups) {
		this.groups = groups;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}	
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public boolean hasAttributes() {
		return attributes;
	}
	public void setAttributes(boolean attributes) {
		this.attributes = attributes;
	}
	public int getStartRow() {
		return startRow;
	}
	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}
	public int getEndRow() {
		return endRow;
	}
	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}
	public boolean printSources() {
		return sources;
	}
	public void setPrintSources(boolean sources) {
		this.sources = sources;
	}	
	public boolean isTokenized() {
		return tokenized;
	}
	public void setTokenized(boolean tokenized) {
		this.tokenized = tokenized;
	}
	public boolean isCaseSensitive() {
		return caseSensitive;
	}
	public void setCaseSensitive(boolean caseSensitive) {
		this.caseSensitive = caseSensitive;
	}
	public boolean isPublic() {
		return isPublic;
	}
	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Map<String, String[]> getRequestParameters() {
		return requestParameters;
	}
	public void setRequestParameters(Map<String, String[]> requestParameters) {
		this.requestParameters = requestParameters;
	}	
	public String[] getFields() {
		return fields;
	}
	public void setFields(String[] fields) {
		this.fields = fields;
	}	
	public Object getNativeQuery() {
		return nativeQuery;
	}
	public void setNativeQuery(Object nativeQuery) {
		this.nativeQuery = nativeQuery;
	}
	public long getDictionary() {
		return dictionary;
	}
	public void setDictionary(long dictionary) {
		this.dictionary = dictionary;
	}	
	public String getParse() {
		return parse;
	}
	public void setParse(String parse) {
		this.parse = parse;
	}
	
	public void fromJson(String in) {
    	JsonReader jsonReader = Json.createReader(new StringReader(in));
		JsonObject jsonObject = jsonReader.readObject();
		fromJsonObject(jsonObject);
    }
	public void fromJsonObject(JsonObject object) {
		if(object.containsKey("startRow")) setStartRow(object.getJsonNumber("startRow").intValue());
    	if(object.containsKey("endRow")) setEndRow(object.getJsonNumber("endRow").intValue());    	
    	if(object.containsKey("isPublic")) setPublic(object.getBoolean("isPublic"));    	
    	if(object.containsKey("searcherId")) setSearcherId(object.getString("searcherId"));    	
    	if(object.containsKey("query")) setQuery(object.getString("query"));
    	if(object.containsKey("context")) setContext(object.getString("context"));
    	if(object.containsKey("operator")) setOperator(object.getString("operator"));
    	if(object.containsKey("attributes")) setAttributes(object.getBoolean("attributes"));
    	if(object.containsKey("sources")) setPrintSources(object.getBoolean("sources"));
    	if(object.containsKey("caseSensitive")) setCaseSensitive(object.getBoolean("caseSensitive"));
    	if(object.containsKey("isPublic")) setPublic(object.getBoolean("isPublic"));
    	if(object.containsKey("dictionary")) setDictionary(object.getJsonNumber("dictionary").longValue());    	
    	if(object.containsKey("user")) {
			User targetUser = new User();
			JsonValue value = object.get("user");
			if(value instanceof JsonObject)
				targetUser.fromJson((JsonObject)value);
			else
				targetUser.setId(((JsonNumber)value).longValue());
			setUser(targetUser);			
		}
    	if(object.containsKey("clauses")) {
    		JsonArray clauseObjs = object.getJsonArray("clauses");
			for(JsonValue clauseValue : clauseObjs) {
				Clause clause = new Clause();
				clause.fromJson((JsonObject)clauseValue);
				getClauses().add(clause);
			}
    	}
    	if(object.containsKey("properties")) {
    		JsonArray propertyObjs = object.getJsonArray("properties");
			for(JsonValue propertyValue : propertyObjs) {
				Property property = new Property((JsonObject)propertyValue);
				getProperties().add(property);
			}
    	}
    	
    	/** Parameters **/
    	if(object.containsKey("parameters")) {
    		JsonArray parameterObjs = object.getJsonArray("parameters");
			for(JsonValue parameterValue : parameterObjs) {
				Parameter parameter = new Parameter();
				parameter.fromJsonObject((JsonObject)parameterValue);
				getParameters().add(parameter);
			}
    	}
		/** Sorts **/
    	if(object.containsKey("sorts")) {
    		JsonArray sortObjs = object.getJsonArray("sorts");
			for(JsonValue sortValue : sortObjs) {
				Sort sort = new Sort();
				sort.fromJsonObject((JsonObject)sortValue);
				getSorts().add(sort);
			}
    	}
		/** QNames **/
    	if(object.containsKey("qnames")) {
    		JsonArray qnameObjs = object.getJsonArray("qnames");
			for(JsonValue qnameValue : qnameObjs) {
				QName qname = new QName();
				qname.fromJsonObject((JsonObject)qnameValue);
				getQnames().add(qname);
			}
    	}
		/** Request Parameters **/
    	if(object.containsKey("requestParameters")) {
    		JsonArray requestParametersObjs = object.getJsonArray("requestParameters");
			for(JsonValue requestParametersValue : requestParametersObjs) {
				String name = ((JsonObject)requestParametersValue).getString("name");
				JsonArray valueObjs = ((JsonObject)requestParametersValue).getJsonArray("values");
				String[] values = new String[valueObjs.size()];
				for(int i=0; i < valueObjs.size(); i++) {
					values[i] = valueObjs.getString(i);
				}
				requestParameters.put(name, values);
			}
    	}
    }
	public JsonObject toJsonObject() {
		JsonObjectBuilder builder = Json.createObjectBuilder();	
		builder.add("startRow", getStartRow());
		builder.add("endRow", getEndRow());
		builder.add("isPublic", isPublic());
		builder.add("searcherId", searcherId);
		builder.add("query", query);
		builder.add("context", context);
		builder.add("operator", operator);
		builder.add("attributes", attributes);
		builder.add("sources", sources);
		builder.add("caseSensitive", caseSensitive);
		builder.add("isPublic", isPublic);
		builder.add("dictionary", dictionary);		
		builder.add("user", getUser().toJsonObject());
		/** Clauses **/
		JsonArrayBuilder clauseBuilder = Json.createArrayBuilder();
		for(Clause clause : clauses) {
			clauseBuilder.add(clause.toJsonObject());
		}
		builder.add("clauses", clauseBuilder);
		/** Properties **/
		JsonArrayBuilder propertyBuilder = Json.createArrayBuilder();
		for(Property property : properties) {
			propertyBuilder.add(property.toJsonObject());
		}
		builder.add("properties", propertyBuilder);
		/** Parameters **/
		JsonArrayBuilder parameterBuilder = Json.createArrayBuilder();
		for(Parameter parameter : parameters) {
			parameterBuilder.add(parameter.toJsonObject());
		}
		builder.add("parameters", parameterBuilder);
		/** Sorts **/
		JsonArrayBuilder sortBuilder = Json.createArrayBuilder();
		for(Sort sort : sorts) {
			sortBuilder.add(sort.toJsonObject());
		}
		builder.add("sorts", sortBuilder);
		/** QNames **/
		JsonArrayBuilder qnameBuilder = Json.createArrayBuilder();
		for(QName qname : qnames) {
			qnameBuilder.add(qname.toJsonObject());
		}
		builder.add("qnames", qnameBuilder);
		/** Request Parameters **/
		JsonArrayBuilder requestParameterBuilder = Json.createArrayBuilder();
		for(String key : requestParameters.keySet()) {
			JsonObjectBuilder requestParameterObjBuilder = Json.createObjectBuilder();
			requestParameterObjBuilder.add("name", key);
			JsonArrayBuilder valueBuilder = Json.createArrayBuilder();
			String[] values = requestParameters.get(key);
			for(String value : values) {
				valueBuilder.add(value);
			}
			requestParameterObjBuilder.add("values", valueBuilder);
			requestParameterBuilder.add(requestParameterObjBuilder);
		}
		builder.add("requestParameters", requestParameterBuilder);
		return builder.build();
	}
}
