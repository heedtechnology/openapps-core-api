package org.heed.openapps.search.dictionary;

import org.heed.openapps.search.Token;

public class AllResultsDefinition extends DefinitionSupport {

	
	public AllResultsDefinition(String name) {
		super(Token.ALL, name, null);
	}
	
	@Override
	public String toString() {
		return "AllResults";
	}
}
