package org.heed.openapps.search.indexing;
import java.util.List;

import org.heed.openapps.entity.indexing.IndexEntity;


public interface IndexingService {

	void index(IndexEntity entity);
	void index(List<IndexEntity> entities);
	void remove(Long id);
	
}
