package org.heed.openapps.search;

import java.util.List;


public interface Searcher {

	//void reload(Entity entity);
	void search(SearchRequest request, SearchResponse response);
	
	List<SearchPlugin> getPlugins();
	
}
