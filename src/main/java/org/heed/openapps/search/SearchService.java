package org.heed.openapps.search;
import java.io.Serializable;
import java.util.List;

import org.heed.openapps.QName;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.scheduling.Job;


public interface SearchService extends Serializable {
	
	int count(SearchRequest request);
	SearchResponse search(SearchRequest request);
	
	Job update(QName qname);
	void update(Entity data);
	void update(List<Entity> data);
	void remove(Long id);
	
	//EntityIndexer getEntityIndexer(String name);
}
