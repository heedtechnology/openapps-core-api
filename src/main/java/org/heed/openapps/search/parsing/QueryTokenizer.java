package org.heed.openapps.search.parsing;
import java.util.List;

import org.heed.openapps.search.Token;


public interface QueryTokenizer {

	void initialize();
	List<Token> tokenize(String query);
	
}
