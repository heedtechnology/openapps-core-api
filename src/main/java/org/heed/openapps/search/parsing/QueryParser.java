package org.heed.openapps.search.parsing;

import org.heed.openapps.search.SearchRequest;


public interface QueryParser {

	SearchRequest parse(SearchRequest request);
		
}
