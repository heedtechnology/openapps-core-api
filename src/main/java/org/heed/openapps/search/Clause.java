package org.heed.openapps.search;

import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import org.heed.openapps.QName;
import org.heed.openapps.entity.Property;


public class Clause {
	public String operator;
	public List<Property> properties = new ArrayList<Property>();
	public List<Parameter> parameters = new ArrayList<Parameter>();
	
	public static final String OPERATOR_OR = "OR";
	public static final String OPERATOR_AND = "AND";
	public static final String OPERATOR_NOT = "NOT";
	
	
	public Clause() {}
	public Clause(String operator, Property p1) {
		this.operator = operator;
		properties.add(p1);
	}
	public Clause(String operator, Property p1, Property p2) {
		this.operator = operator;
		properties.add(p1);
		properties.add(p2);
	}
	public Clause(String operator, Property p1, Property p2, Property p3) {
		this.operator = operator;
		properties.add(p1);
		properties.add(p2);
		properties.add(p3);
	}
	
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public void addProperty(QName name, String type, String value) {
		properties.add(new Property(name, type, value));
	}
	public void addProperty(Property parm) {
		properties.add(parm);
	}
	public List<Property> getProperties() {
		return properties;
	}
	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}	
	public List<Parameter> getParameters() {
		return parameters;
	}
	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}
	public void addParameter(Parameter parameter) {
		parameters.add(parameter);
	}
	
	public void fromJson(JsonObject object) {
		if(object.containsKey("operator")) setOperator(object.getString("operator"));
		if(object.containsKey("properties")) {
			JsonArray properties = object.getJsonArray("properties");
			for(JsonValue propertyValue : properties) {
				Property property = new Property((JsonObject)propertyValue);
				getProperties().add(property);
			}
		}
		if(object.containsKey("parameters")) {
			JsonArray parameters = object.getJsonArray("parameters");
			for(JsonValue propertyValue : parameters) {
				Parameter parameter = new Parameter();
				parameter.fromJsonObject((JsonObject)propertyValue);
				getParameters().add(parameter);
			}
		}
	}
	public JsonObject toJsonObject() {
		JsonObjectBuilder builder = Json.createObjectBuilder();	
		builder.add("operator", getOperator());
		JsonArrayBuilder propertyBuilder = Json.createArrayBuilder();
		for(Property property : properties) {
			propertyBuilder.add(property.toJsonObject());
		}
		builder.add("properties", propertyBuilder);
		JsonArrayBuilder parameterBuilder = Json.createArrayBuilder();
		for(Parameter parameter : parameters) {
			parameterBuilder.add(parameter.toJsonObject());
		}
		builder.add("parameters", parameterBuilder);
		return builder.build();
	}
}
