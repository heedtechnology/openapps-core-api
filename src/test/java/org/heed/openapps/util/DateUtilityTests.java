package org.heed.openapps.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.junit.Test;


public class DateUtilityTests {
	private static final SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //2015-06-17 12:18:14
	
	@SuppressWarnings("deprecation")
	@Test
	public void testChangeTimezone() {
		Date fromDate = new Date(2015 - 1900, 6, 6, 12, 8);        		
		TimeZone calTimezone = TimeZone.getTimeZone("America/Los_Angeles");
		TimeZone nycTimezone = TimeZone.getTimeZone("America/New_York");
		Date toDate = DateUtility.changeTimezone(fromDate, calTimezone, nycTimezone);
		System.out.println("converted " + fmt.format(fromDate) + " to " + fmt.format(toDate));
	}

}
